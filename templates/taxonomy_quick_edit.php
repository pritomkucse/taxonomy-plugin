<fieldset>
    <div class="inline-edit-col inline-edit-col-taxonomy-my-color">
        <label>
            <span class="title"><?php _e( 'Creator', 'my-taxonomy-plugin' ); ?></span>
            <span class="input-text-wrap"><input type="text" name="<?php echo esc_attr($column_name); ?>" class="ptitle" value=""></span>
        </label>
    </div>
</fieldset>