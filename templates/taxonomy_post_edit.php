<?php
global $taxonomy_data_map;
$taxonomy_class = $taxonomy_data_map['class'];
$taxonomy_options = $taxonomy_data_map['options'];
$taxonomy_selected = $taxonomy_data_map['selected'];
$value_field = $taxonomy_data_map['value_field'];
?>

<div class="<?= $taxonomy_class ?>-editor-on-post">
    <h2>
        Colors
    </h2>
    <?php foreach ($taxonomy_options as $o) { ?>
        <div class="<?= $taxonomy_class ?>-taxonomies__hierarchical-terms-list" tabindex="0" role="group" aria-label="Available Colors">
            <?php $_checked = isset($taxonomy_selected[$o->term_id]) ? 'checked' : ''; ?>
            <div class="<?= $taxonomy_class ?>-taxonomies__hierarchical-terms-choice">
                <input
                        id="<?= $taxonomy_class ?>-taxonomies-hierarchical-term-<?= $o->term_id ?>"
                        class="<?= $taxonomy_class ?>-taxonomies__hierarchical-terms-input"
                        type="checkbox" value="<?= $o->{$value_field} ?>"
                        name="<?= $taxonomy_class ?>[]" <?= $_checked ?>
                />
                <label for="<?= $taxonomy_class ?>-taxonomies-hierarchical-term-<?= $o->term_id ?>"><?= esc_attr($o->name) ?></label>
            </div>
        </div>
    <?php } ?>
    <div style="clear:both"></div>
</div>