<?php
global $taxonomy_data_map;
$terms = $taxonomy_data_map['options'];
$value_field = $taxonomy_data_map['value_field'];
$selected = $taxonomy_data_map['selected'];
?>
<fieldset class="inline-edit-col-left my_taxonomy_plugin_color-quick-editor">
    <div class="inline-edit-col">
        <span class="title"><?= __('Colors') ?></span>
        <select name='<?= $taxonomy_data_map['name'] ?>[]' id='<?= $taxonomy_data_map['name'] ?>' multiple>
            <?php
            foreach ($terms as $term) {
                $_checked = isset($selected[$term->term_id]) ? 'selected' : '';
                echo "<option value='{$term->{$value_field}}' {$_checked}>{$term->name}</option>\n";
            }
            ?>
        </select>
    </div>
</fieldset>