<table class="form-table">
    <tbody>
    <tr class="form-field form-required term-name-wrap">
        <th scope="row"><label for="my-taxonomy-plugin-color-f1"><?php _e('Creator'); ?></label></th>
        <td>
            <input name="my-taxonomy-plugin-color-f1" id="my-taxonomy-plugin-color-f1" type="text" value="<?= esc_attr($my_taxonomy_plugin_color_f1) ?>" size="70" />
            <p class="description"><?php _e('Title display in creator is limited to 70 chars'); ?></p>
        </td>
    </tr>
    </tbody>
</table>