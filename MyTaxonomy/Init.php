<?php
namespace MyTaxonomy;

class Init {
    public static function register_services() {
        add_action('init', array(self::class, 'create_topics_hierarchical_taxonomy'), 0);
        add_action('admin_init', array(Init::class, 'check_if_plugin'));
        add_action("wp_enqueue_scripts", array(self::class, "my_add_admin_scripts"));
        add_action('admin_enqueue_scripts', array(self::class, 'my_add_admin_scripts'));

        $taxonomy1 = 'manage_edit-' . self::getTaxonomyName() . '_columns';
        add_filter($taxonomy1, array(self::class, 'register_category_columns'));

        $taxonomy2 = 'manage_' . self::getTaxonomyName() . '_custom_column';
        add_filter($taxonomy2, array(self::class, 'category_column_display'), 10, 3);

        add_action('quick_edit_custom_box', array(self::class, 'quick_edit_taxonomy_field'), 10, 2);
        $taxonomy3 = 'edited_' . self::getTaxonomyName();
        add_action($taxonomy3, array(self::class, 'quick_edit_save_category_field'));

        add_action(self::getTaxonomyName() . '_add_form_fields', array(
            self::class, 'taxonomy_metadata_add'
        ), 10, 1);
        add_action(self::getTaxonomyName() . '_edit_form', array(
            self::class, 'taxonomy_metadata_edit'
        ), 10, 1);
        add_action('created_' . self::getTaxonomyName(), array(
            self::class, 'save_taxonomy_metadata'
        ), 10, 1);
        add_action('edited_' . self::getTaxonomyName(), array(
            self::class, 'save_taxonomy_metadata'
        ), 10, 1);

        add_filter('manage_posts_columns', array(self::class, 'add_post_fake_column'), 10, 2);
        add_filter('manage_edit-post_columns', array(self::class, 'remove_post_fake_column'));
        add_action('quick_edit_custom_box', array(self::class, 'add_post_quick_edit_box'), 10, 2);
        add_action('save_post', array(self::class, "my_taxonomy_post_save_callback"), 1, 2);

        $action_name = 'plugin_my_personal_taxonomy';
        add_action ('wp_ajax_' . $action_name, array(self::class, 'ajax_call_your_function'));
        add_action ('wp_ajax_nopriv_' . $action_name, array(self::class, 'ajax_call_your_function'));

        add_filter('the_content', array(self::class, 'my_the_content'));
        add_action('pre_get_posts', array(self::class, 'my_pre_get_posts'), 33, 1);
        add_filter('posts_where', array(self::class, 'my_posts_where'), 10, 2);
        add_filter('the_posts', array(self::class, 'my_post_filtering'));
    }

    public static function my_post_filtering($posts) {
        if (!is_admin() && !is_feed() && (is_single() || is_page()) && count($posts) == 1) {
            if (in_array($posts[0]->post_type, self::available_post_types())) {
                $args = array(
                    'post__in' => array($posts[0]->ID),
                    'suppress_filters' => 1
                );
                $the_query = new \WP_Query($args);
                return $the_query->post_count == 1 ? $posts : array();
            }
        }
        return $posts;
    }

    public static function my_posts_where($where, $query) {
        return $where;
    }

    public static function my_pre_get_posts($query) {
        if (is_admin()) {
            return;
        }
        //self::pre($query); exit;
        self::apply_filter_to_wp_query($query);
    }

    public static function my_the_content($c) {
        if (!is_admin() && !is_feed() && (is_single() || is_page())) {
            global $post;
            $allow = !is_null($post) && property_exists($post, "post_type") && in_array($post->post_type, self::available_post_types());
            if ($allow) {
                $content = array();
                foreach(wp_get_post_terms($post->ID, self::getTaxonomyName()) as $o) {
                    $content[] = $o->name;
                }
                $c = $c . "<div class=\"my_taxonomy_plugin_color-post-display\">
                                <div class=\"row\">
                                <label class=\"label\">Colors</label>
                                <span class=\"value\">" . implode(', ', $content) . "</span>
                            </div>
                        </div>";
            }
        }
        return $c;
    }

    private static function available_post_types() {
        return array('post', 'page');
    }

    private static function apply_filter_to_wp_query($query) {
        $tax_query = (array) $query->get('tax_query');
        $tax_query[] = array(
            'relation' => 'or',
            array(
                'taxonomy' => self::getTaxonomyName(),
                'field' => 'slug',
                'terms' => array('red'),
                'operator'=> 'IN'
            ),
            array(
                'relation' => 'and',
                array(
                    'taxonomy' => self::getTaxonomyName(),
                    'field' => 'slug',
                    'terms' => array('black'),
                    'operator'=> 'IN'
                ),
                array(
                    'taxonomy' => self::getTaxonomyName(),
                    'field' => 'slug',
                    'terms' => array('orange'),
                    'operator'=> 'IN'
                )
            )
        );
        $query->tax_query = $tax_query;
        $query->set('tax_query', $tax_query);
    }

    public static function ajax_call_your_function() {
        $case = isset($_REQUEST['case']) ? $_REQUEST['case'] : '';
        switch ($case) {
            case 'post-quick-edit':
                $id = isset($_REQUEST['id']) ? doubleval($_REQUEST['id']) : 0;
                if ($id > 0) {
                    $GLOBALS['post_id_from_quick_edit_action'] = $id;
                    self::add_post_quick_edit_box(null, null);
                    wp_die();
                }
                break;
        }
        header("HTTP/1.1 404");
        echo __("Not Found");
        exit;
    }

    public static function add_post_fake_column($posts_columns, $post_type) {
        $posts_columns['fake_column_taxonomy_color'] = 'Fake Column (Invisible)';
        return $posts_columns;
    }

    public static function remove_post_fake_column($posts_columns) {
        unset($posts_columns['fake_column_taxonomy_color']);
        return $posts_columns;
    }

    public static function add_post_quick_edit_box($column_name, $post_type) {
        if (true) {
            $taxonomy = get_taxonomy(self::getTaxonomyName());

            $taxonomy_options = get_terms(array(
                'taxonomy' => self::getTaxonomyName(),
                'hide_empty' => false,
            ));

            if (count($taxonomy_options) > 0) {
                $post_terms = [];
                $post_id = isset($GLOBALS['post_id_from_quick_edit_action']) ? $GLOBALS['post_id_from_quick_edit_action'] : get_the_ID();
                foreach(wp_get_post_terms($post_id, self::getTaxonomyName()) as $o) {
                    $post_terms[$o->term_id] = 1;
                }

                global $taxonomy_data_map;
                $taxonomy_data_map = [];
                $taxonomy_data_map['name'] = self::getTaxonomyName();
                $taxonomy_data_map['value_field'] = $taxonomy->hierarchical ? 'term_id' : 'name';
                $taxonomy_data_map['options'] = $taxonomy_options;
                $taxonomy_data_map['selected'] = $post_terms;

                include_once MY_TAXONOMY_PLUGIN_DIR . 'templates/taxonomy_post_quick_edit.php';
                wp_nonce_field( MY_TAXONOMY_PLUGIN_NONCE_FIELD_NAME, MY_TAXONOMY_PLUGIN_NONCE_FIELD_NAME );
            }
        }
    }

    public static function getTaxonomyName() {
        return MY_TAXONOMY_PLUGIN_CORE . 'color';
    }

    public static function check_if_plugin() {
        if (is_plugin_active(MY_TAXONOMY_PLUGIN_NAME)) {
            add_meta_box(
                MY_TAXONOMY_PLUGIN_CORE . 'metabox1', __( 'Choose Colors', MY_TAXONOMY_PLUGIN_CORE ),
                array(Init::class, 'my_meta_box_data_callback'), 'post'
            );
        }
    }

    public static function my_taxonomy_post_save_callback($id, $post) {
        if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) || !current_user_can('edit_page', $id)) {
            return $id;
        }
        if (!isset($_POST[MY_TAXONOMY_PLUGIN_NONCE_FIELD_NAME])) {
            return $id;
        }
        if (!wp_verify_nonce($_POST[MY_TAXONOMY_PLUGIN_NONCE_FIELD_NAME], MY_TAXONOMY_PLUGIN_NONCE_FIELD_NAME)) {
            return $id;
        }
        if (!current_user_can( 'edit_post', $id)) {
            return $id;
        }

        $taxonomy = get_taxonomy(self::getTaxonomyName());
        $field = $taxonomy->hierarchical ? 'term_id' : 'name';

        $taxonomy_options = array();
        foreach(get_terms(array('taxonomy' => self::getTaxonomyName(), 'hide_empty' => false)) as $o) {
             $taxonomy_options[strval($o->{$field})] = 1;
        }

        if (count($taxonomy_options)) {
            $options = array();
            foreach((isset($_POST[self::getTaxonomyName()]) ? $_POST[self::getTaxonomyName()] : array()) as $i) {
                if (isset($taxonomy_options[$i])) {
                    array_push($options, $i);
                }
            }
            wp_set_post_terms($id, $options, self::getTaxonomyName());
        }
        else {
            wp_set_post_terms($id, array(), self::getTaxonomyName());
        }

        return $id;
    }

    public static function my_meta_box_data_callback() {
        global $post;
        $taxonomy_options = get_terms(array(
            'taxonomy' => self::getTaxonomyName(),
            'hide_empty' => false,
        ));
        if (count($taxonomy_options) > 0) {
            $post_terms = [];
            foreach(wp_get_post_terms($post->ID, self::getTaxonomyName()) as $o) {
                $post_terms[$o->term_id] = 1;
            }

            $taxonomy = get_taxonomy(self::getTaxonomyName());

            global $taxonomy_data_map;
            $taxonomy_data_map = [];
            $taxonomy_data_map['class'] = self::getTaxonomyName();
            $taxonomy_data_map['options'] = $taxonomy_options;
            $taxonomy_data_map['selected'] = $post_terms;
            $taxonomy_data_map['value_field'] = $taxonomy->hierarchical ? 'term_id' : 'name';

            include_once MY_TAXONOMY_PLUGIN_DIR . 'templates/taxonomy_post_edit.php';
            wp_nonce_field( MY_TAXONOMY_PLUGIN_NONCE_FIELD_NAME, MY_TAXONOMY_PLUGIN_NONCE_FIELD_NAME );
        }
    }

    public static function create_topics_hierarchical_taxonomy() {
        register_taxonomy(self::getTaxonomyName(), self::available_post_types(), array(
            'hierarchical' => false,
            'query_var' => true,
            'rewrite' => array('slug' => 'taxonomy-color'),
            'show_ui' => true,
            'show_in_nav_menus' => true,
            'show_admin_column' => true,
            'show_in_quick_edit'=> false,
            'public'            => true,
            'labels' => array(
                'name' => __('Colors'),
                'singular_name' => __('Color'),
                'search_items' => __('Search Color'),
                'popular_items' => null,
                'all_items' => __('All Colors'),
                'edit_item' => __('Edit Color'),
                'update_item' => __('Update Color'),
                'add_new_item' => __('Add New Color'),
                'new_item_name' => __('New Color Name'),
                'separate_items_with_commas' => null,
                'add_or_remove_items' => null,
                'choose_from_most_used' => null,
                'back_to_items' => __('&larr; Back to Colors'),
            )
        ));

        //Default taxonomy will be created on initialization
        wp_insert_term("Black", MY_TAXONOMY_PLUGIN_CORE . "color");
        wp_insert_term("Blue", MY_TAXONOMY_PLUGIN_CORE . "color");
        wp_insert_term("Yellow", MY_TAXONOMY_PLUGIN_CORE . "color");
        wp_insert_term("Red", MY_TAXONOMY_PLUGIN_CORE . "color");
    }

    public function quick_edit_taxonomy_field($column_name, $screen) {
        if ( $screen != 'edition' && $column_name != 'my-taxonomy-plugin-color-f1' ) {
            return false;
        }
        include_once MY_TAXONOMY_PLUGIN_DIR . "templates/taxonomy_quick_edit.php";
    }

    public static function my_add_admin_scripts() {
        global $pagenow;

        $taxonomy = MY_TAXONOMY_PLUGIN_CORE . 'color';
        /*if($pagenow == 'edit-tags.php' && (isset($_GET['taxonomy']) && $_GET['taxonomy'] == $taxonomy) && !isset($_GET['action']))
        {*/

        //}
        wp_register_script(
            'MY_TAXONOMY_PLUGIN_CORE',
            MY_TAXONOMY_PLUGIN_URL . 'assets/script7.js',
            array('jquery')
        );

        $params = array(
            'BASE_URL' => MY_TAXONOMY_PLUGIN_URL,
            'ADMIN_URL' => admin_url()
        );
        wp_localize_script( 'MY_TAXONOMY_PLUGIN_CORE', 'MY_TAXONOMY_PLUGIN_CORE', $params);

        wp_enqueue_script('MY_TAXONOMY_PLUGIN_CORE');

        wp_enqueue_style(
            "MY_TAXONOMY_PLUGIN_CORE", MY_TAXONOMY_PLUGIN_URL . "assets/style7.css"
        );
    }

    public function quick_edit_save_category_field($term_id) {
        if (isset($_POST['my-taxonomy-plugin-color-f1'])) {
            update_term_meta(
                $term_id, 'my-taxonomy-plugin-color-f1',
                $_POST['my-taxonomy-plugin-color-f1']
            );
        }
    }

    public static function category_column_display($string, $column_name, $term_id) {
        return esc_html(get_term_meta($term_id, $column_name, true));
    }

    public static function register_category_columns($columns) {
        $columns['my-taxonomy-plugin-color-f1'] = __( 'Creator', 'my-taxonomy-plugin' );
        return $columns;
    }

    public static function taxonomy_metadata_add($tag) {
        if (current_user_can( 'publish_posts')) {
            include_once MY_TAXONOMY_PLUGIN_DIR . "templates/taxonomy_add.php";
        }
    }

    public static function taxonomy_metadata_edit($tag) {
        if (current_user_can( 'publish_posts')) {
            ob_start();
            global $my_taxonomy_plugin_color_f1;
            $my_taxonomy_plugin_color_f1 = get_term_meta($tag->term_id, 'my-taxonomy-plugin-color-f1', true);
            include_once MY_TAXONOMY_PLUGIN_DIR . "templates/taxonomy_edit.php";
            $content = ob_get_contents();
            ob_end_clean();
            echo $content;
        }
    }

    public static function save_taxonomy_metadata($term_id) {
        if ( isset($_POST['my-taxonomy-plugin-color-f1']) ) {
            update_term_meta($term_id, 'my-taxonomy-plugin-color-f1', esc_attr($_POST['my-taxonomy-plugin-color-f1']));
        }
    }

    public static function activate() {
        flush_rewrite_rules();
    }

    public static function deactivate() {
        flush_rewrite_rules();
    }

    public static function pre($o) {
        echo '<pre>'; print_r($o); echo '</pre>';
    }
}