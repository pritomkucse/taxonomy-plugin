<?php
/**
 * @package my-taxonomy-plugin
 */

/*
Plugin Name: My Taxonomy Plugin
Description: My Taxonomy plugin
Version: 1.0.0
Author: Pritom Kumar
License: GPLv2 or later
*/

if ( !function_exists( 'add_action' ) ) {
    echo 'Unable to proceed';
    exit;
}
require_once "MyTaxonomy/Init.php";
define("MY_TAXONOMY_PLUGIN_NAME", "my-taxonomy-plugin/my-taxonomy-plugin.php");
define("MY_TAXONOMY_PLUGIN_CORE", "my_taxonomy_plugin_");
define("MY_TAXONOMY_PLUGIN_DIR", plugin_dir_path(__FILE__));
define("MY_TAXONOMY_PLUGIN_URL", plugin_dir_url(__FILE__));
define("MY_TAXONOMY_PLUGIN_NONCE_FIELD_NAME", MY_FIRST_PLUGIN_CORE . "nonce_35g45gdeesldj4");

register_activation_hook(__FILE__, array(MyTaxonomy\Init::class, "activate"));
register_activation_hook(__FILE__, array(MyTaxonomy\Init::class, "deactivate"));
MyTaxonomy\Init::register_services();

function log_file_name($start = 0) {
    try {
        $bt = debug_backtrace();
        $fileAndLine = "";
        for ($i = $start; $i < 100; $i++) {
            $row = $bt[$i];
            if (isset($row["file"]) && isset($row["line"])) {
                $fileAndLine = $row["file"] . "::" . $row["line"];
                $i = 50;
            }
        }
        return $fileAndLine;
    }
    catch (\Exception $ex) {
        return "";
    }
}