jQuery(document).ready(function(){
    var $ = jQuery, body = $(document.body);

    if (typeof inlineEditTax !== 'undefined') {
        (function ($, inlineEditTax) {
            // inlineEditTax does not invoke any events, but does ensure to stop
            // propagation to all other event handlers; swap it out.
            inlineEditTax.editPreMyPlugin = inlineEditTax.edit;
            inlineEditTax.edit = function (link) {
                // Invoke original edit event handler.
                this.editPreMyPlugin(link);

                var tr = $(link).closest('tr[id]');

                var tag_id = tr.attr('id');
                var field1_value = tr.find(".column-my-taxonomy-plugin-color-f1").text();
                $(':input[name="my-taxonomy-plugin-color-f1"]', '.inline-edit-row').val(field1_value);

                return false;
            }
        })($, inlineEditTax);
    }
    if (typeof inlineEditPost !== 'undefined') {
        (function ($, inlineEditPost) {
            // inlineEditTax does not invoke any events, but does ensure to stop
            // propagation to all other event handlers; swap it out.
            inlineEditPost.realOne = inlineEditPost.edit;

            inlineEditPost.edit = function (link) {
                // Invoke original edit event handler.
                this.realOne(link);

                var id = inlineEditPost.getId(link), tr = $(link).closest("table").find("tr#edit-" + id);
                var qe = tr.find(".my_taxonomy_plugin_color-quick-editor");
                if (qe.length) {
                    $.ajax ({
                        url: MY_TAXONOMY_PLUGIN_CORE.ADMIN_URL + '/admin-ajax.php',
                        type: 'POST',
                        dataType: 'HTML',
                        data: {
                            action: 'plugin_my_personal_taxonomy',
                            case: 'post-quick-edit',
                            id: id
                        },
                        success: function (resp) {
                            qe.html($(resp).html());
                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                        },
                    });
                }
                return false;
            }
        })($, inlineEditPost);
    }
});